
// This is called when a new player is registered
handlers.NewPlayerRegistered = function (args, context) {
    var request = {
        PlayFabId: currentPlayerId, Statistics: [{
                StatisticName: "Level",
                Value: 1
            },
            {
                StatisticName: "Units",
                Value: 1
            },
            {
                StatisticName: "Power",
                Value: 1
            },
            {
                StatisticName: "Kills",
                Value: 0
            },
            {
                StatisticName: "RecruitTime",
                Value: 0
            }]
    };
    
    server.UpdatePlayerStatistics(request);
};

handlers.getReward = function (args, context) {
    
    let statistics = server.GetPlayerStatistics({ PlayFabId: currentPlayerId }).Statistics;
    
    var totalKills = statistics.find(s => s.StatisticName === "Kills").Value + args.Kills;
    var level = statistics.find(s => s.StatisticName === "Level").Value;
    
    var softCurrency = args.Kills * (Math.floor(Math.random() * 51) + 50);
    var hardCurrency = 0;
    var nextLevel = level * 10;
    
    if(totalKills >= nextLevel)
    {
        level += 1;
        softCurrency += 500 * level;
        hardCurrency += 10 * level;
        totalKills -= nextLevel;
    }
    
    var request = {
        PlayFabId: currentPlayerId, Statistics: [{
                StatisticName: "Level",
                Value: level
            },
            {
                StatisticName: "Kills",
                Value: totalKills
            }]
    };
    
    server.UpdatePlayerStatistics(request);
    server.AddUserVirtualCurrency({PlayFabId: currentPlayerId, VirtualCurrency: "SC", Amount: softCurrency});
    server.AddUserVirtualCurrency({PlayFabId: currentPlayerId, VirtualCurrency: "HC", Amount: hardCurrency});
    
    return {Status: 1001, Gold: softCurrency, Gem: hardCurrency, Level: level, Kills: totalKills};
};


handlers.recruitNewUnit = function (args, content) {
    
    let statistics = server.GetPlayerStatistics({ PlayFabId: currentPlayerId }).Statistics;
    var units = statistics.find(s => s.StatisticName === "Units").Value;
    var recruitTime = statistics.find(s => s.StatisticName === "RecruitTime").Value;
    
    if(units == 6)
    {
        return {Status: 1500, Error: "Trying to Upgrade Max Level Stat"};
    }
    
    if(recruitTime > 0)
    {
        return CheckRecruit({SpeedUp: 0});
    }
    
    var upgradePrice = (500 * Math.pow(2, units - 1));
    
    let virtualCurrencyBalance = server.GetPlayerCombinedInfo({
        "PlayFabId": currentPlayerId,
        "InfoRequestParameters": {
            "GetUserVirtualCurrency": true
            }
        }).InfoResultPayload.UserVirtualCurrency.SC;
        
    if (virtualCurrencyBalance < upgradePrice)
    {
        return {Status: 1500, Error: "Not Enought Currency"};
    }
    
    server.SubtractUserVirtualCurrency({
            Amount: upgradePrice,
            PlayFabId: currentPlayerId,
            VirtualCurrency: "SC"
        });
        
    var now = new Date().getTime();
    
    //Wait time in seconds (15min * 2 ^ (units - 1))
    var waitTime = (60 * 15 * Math.pow(2, units - 1));
    
    var finishTime = (Math.floor(now/1000) + waitTime)
    
    var request = {
        PlayFabId: currentPlayerId, Statistics: [{
                StatisticName: "RecruitTime",
                Value: finishTime
            }]
    };
    
    server.UpdatePlayerStatistics(request);

    return {Status: 1002, Time: waitTime, SoftCurrency: virtualCurrencyBalance - upgradePrice};
}

function CheckRecruit(args) {
    
    let statistics = server.GetPlayerStatistics({ PlayFabId: currentPlayerId }).Statistics;
    var units = statistics.find(s => s.StatisticName === "Units").Value;
    var recruitTime = statistics.find(s => s.StatisticName === "RecruitTime").Value;
    
    if(units == 6)
    {
        return {Status: 1500, Error: "Trying to Upgrade Max Level Stat"};
    }
    
    if(recruitTime == 0)
    {
        return {Status: 1500, Error: "There is no upgrade in progress"};
    }
    
    var now = new Date().getTime();
    
    var remainingTime = Math.max(recruitTime - (Math.floor(now/1000)), 0);
    var speedUpPrice = 0;
    
    let virtualCurrencyBalance = server.GetPlayerCombinedInfo({
        "PlayFabId": currentPlayerId,
        "InfoRequestParameters": {
            "GetUserVirtualCurrency": true
            }
        }).InfoResultPayload.UserVirtualCurrency.HC;
    

    //make sure the player has enough virtualCurrency
    if(args.SpeedUp == 1)
    {
        //speed up cost 1 gem per minute
        speedUpPrice = Math.floor(remainingTime/60);
        
        if (virtualCurrencyBalance < speedUpPrice)
        {
            return {Status: 1500, Error: "Not Enought Currency"};
        }
        
        server.SubtractUserVirtualCurrency({
            Amount: speedUpPrice,
            PlayFabId: currentPlayerId,
            VirtualCurrency: "HC"
        });
    }
    else if(remainingTime > 30)
    {
        return {Status: 1501, Error: "Not Ready", Time: remainingTime};
    }
    
    
    remainingTime = 0;
    units += 1;
    
    var request = {
            PlayFabId: currentPlayerId, Statistics: [{
                    StatisticName: "RecruitTime",
                    Value: 0
                },
                {
                    StatisticName: "Units",
                    Value: units
                }]
        };
        
    server.UpdatePlayerStatistics(request);
    
    return {Status: 1003, Time: remainingTime, Units: units, Gem: virtualCurrencyBalance - speedUpPrice};
}

handlers.upgradePower = function (args, content) {
    
    let statistics = server.GetPlayerStatistics({ PlayFabId: currentPlayerId }).Statistics;
    var power = statistics.find(s => s.StatisticName === "Power").Value;
    
    if(power == 9)
    {
        return {Status: 1500, Error: "Trying to Upgrade Max Level Stat"};
    }
    
    var upgradePrice = (100 * Math.pow(2, power - 1));
    
    let virtualCurrencyBalance = server.GetPlayerCombinedInfo({
        "PlayFabId": currentPlayerId,
        "InfoRequestParameters": {
            "GetUserVirtualCurrency": true
            }
        }).InfoResultPayload.UserVirtualCurrency.SC;
        
    if (virtualCurrencyBalance < upgradePrice)
    {
        return {Status: 1500, Error: "Not Enought Currency"};
    }
    
    power+=1;
    
    server.SubtractUserVirtualCurrency({
            Amount: upgradePrice,
            PlayFabId: currentPlayerId,
            VirtualCurrency: "SC"
        });
    
    var request = {
            PlayFabId: currentPlayerId, Statistics: [{
                    StatisticName: "Power",
                    Value: power
                }]
        };
        
    server.UpdatePlayerStatistics(request);
    
    return {Status: 1004, Power: power, SoftCurrency: virtualCurrencyBalance - upgradePrice};
}

handlers.recruitNewUnitNow = function (args, content) {
    return CheckRecruit(args);
}
