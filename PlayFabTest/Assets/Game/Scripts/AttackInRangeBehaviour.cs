﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AttackInRangeBehaviour : MonoBehaviour
{
    [SerializeField] private float secondsBetweenAttacks = 1;
    [SerializeField] private Animator animator;

    private HashSet<EnemyBehaviour> enemyInRange = new HashSet<EnemyBehaviour>();

    IEnumerator Start()
    {
        while (true)
        {
            if (enemyInRange.Count > 0)
            {
                animator.SetTrigger("Attack");
                var enemy = enemyInRange.First();
                transform.LookAt(enemy.transform);
                yield return new WaitForSeconds(1.5f);
                enemy.Damage();
                if (enemy.IsDead)
                {
                    enemyInRange.Remove(enemy);
                }
                yield return new WaitForSeconds(secondsBetweenAttacks);
            }
            else
            {
                yield return new WaitUntil(() => enemyInRange.Count > 0);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        var enemy = other.GetComponent<EnemyBehaviour>();
        if (enemy != null)
        {
            enemyInRange.Add(enemy);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var enemy = other.GetComponent<EnemyBehaviour>();
        if (enemy != null)
        {
            enemyInRange.Remove(enemy);
        }
    }
}
