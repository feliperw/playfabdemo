﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntValueUI : MonoBehaviour
{
    [SerializeField] private IntValue value;
    [SerializeField] private Text textField;

    [SerializeField] private string prefix;
    [SerializeField] private string suffix;
    private void OnEnable()
    {
        textField.text = $"{prefix}{value.Value}{suffix}";
        value.OnValueChanged += UpdateValue;
    }

    private void OnDisable()
    {
        value.OnValueChanged -= UpdateValue;
    }

    private void UpdateValue(int previewsValue, int newValue)
    {
        textField.text = $"{prefix}{newValue}{suffix}";
    }
}
