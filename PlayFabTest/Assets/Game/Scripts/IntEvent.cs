﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewIntEvent", menuName = "Scriptables/Events/Int Event")]
public class IntEvent : ScriptableEventGeneric<int>
{}