﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitsActivator : MonoBehaviour
{
    [SerializeField] private List<GameObject> units;
    [SerializeField] private IntValue unitsLevel;
    private void OnEnable()
    {
        unitsLevel.OnValueChanged += OnStatChanged;
    }

    private void OnDisable()
    {
        unitsLevel.OnValueChanged -= OnStatChanged;
    }

    private void OnStatChanged(int oldValue, int newValue)
    {
        for (int i = 0; i < units.Count; i++)
        {
            units[i].SetActive(i < unitsLevel.Value);
        }
    }
}
