﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradePanel : MonoBehaviour
{
    [SerializeField] private IntValue stat;
    [SerializeField] private int maxLevel;
    [SerializeField] private int costMultiplier;
    [SerializeField] private Text currencyCost;
    [SerializeField] private Canvas upgradeCanvas;
    [SerializeField] private PlayFabStats playFabStats;
    
    
    [SerializeField, Header("Timed Update")] private Text timeCost;
    [SerializeField] private IntEvent startTimer;
    [SerializeField] private Canvas ongoingUpgradeCanvas;
    
    private int currentTimer = 0;
    [SerializeField] private Text timer;
    [SerializeField] private Text speedUpCost;

    private void OnEnable()
    {
        stat.OnValueChanged += OnStatChanged;

        if (startTimer != null)
        {
            startTimer.OnRaise += OnStartUpgrade;
        }
    }

    private void OnDisable()
    {
        stat.OnValueChanged -= OnStatChanged;

        if (startTimer != null)
        {
            startTimer.OnRaise -= OnStartUpgrade;
        }
    }

    private void OnStatChanged(int previewValue, int newValue)
    {
        upgradeCanvas.enabled = newValue < maxLevel;

        if (upgradeCanvas.enabled)
        {
            currencyCost.text = $"Gold: {costMultiplier * Mathf.Pow(2, newValue - 1)}";
            
            if (timeCost != null)
            {
                timeCost.text = $"Time: {SecondsToTime(Mathf.RoundToInt(60 * 15 * Mathf.Pow(2, newValue - 1)))}";
            }
        }

        if (ongoingUpgradeCanvas != null)
        {
            ongoingUpgradeCanvas.enabled = false;
        }
    }

    private void OnStartUpgrade(int value)
    {
        if (ongoingUpgradeCanvas == null) return;
        
        upgradeCanvas.enabled = false;
        ongoingUpgradeCanvas.enabled = true;

        currentTimer = value;

        UpdateTimerUI();
        
        StopAllCoroutines();

        StartCoroutine(StartUpgradeTimer());
    }

    string SecondsToTime(int seconds)
    {
        var h = seconds / 3600; //hour
        var m = (seconds % 3600) / 60; // minute
        var s = (seconds % 3600) % 60; // second
        var t = " ";
        if (h > 0)
        {
            t += $"{h}h ";
        }
        if (m > 0)
        {
            t += $"{m}m ";
        }
        if (s > 0)
        {
            t += $"{s}s ";
        }

        return t;
    }

    private void UpdateTimerUI()
    {
        timer.text = SecondsToTime(currentTimer);
        speedUpCost.text = $"{Mathf.Floor(currentTimer / 60)}";
    }

    IEnumerator StartUpgradeTimer()
    {
        while (currentTimer > 0)
        {
            yield return new WaitForSeconds(1);
            currentTimer -= 1;
            UpdateTimerUI();
        }
        
        yield return new WaitForSeconds(0.5f);
        playFabStats.CheckRecruit();
    }
}
