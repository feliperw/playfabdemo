﻿using System;
using UnityEngine;

public abstract class ScriptableVariable<T> : ScriptableObject
{
    //OnValueChange(preview value / new value)
    public Action<T, T> OnValueChanged;
    
    [SerializeField] private T value;

    public T Value
    {
        get
        {
            return value;
        }

        set
        {
            var previewValue = this.value;
            this.value = value;
            OnValueChanged?.Invoke(previewValue, value);
        }
    }
}