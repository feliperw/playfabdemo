﻿using UnityEngine;

[CreateAssetMenu(fileName = "IntValue", menuName = "Scriptables/Variables/Int")]
public class IntValue : ScriptableVariable<int>
{
}
