﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageBoxController : MonoBehaviour
{
    [SerializeField] private GameObject popup;
    [SerializeField] private Text message;
    [SerializeField] private StringEvent showMessageEvent;
    
    void OnEnable()
    {
        showMessageEvent.OnRaise += ShowMessage;
    }

    private void OnDisable()
    {
        showMessageEvent.OnRaise -= ShowMessage;
    }

    private void ShowMessage(string messageText)
    {
        message.text = messageText;
        popup.SetActive(true);
    }
}
