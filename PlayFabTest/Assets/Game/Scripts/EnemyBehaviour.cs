﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Lean.Pool;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour, IPoolable
{
    [SerializeField] private int maxHP = 100;
    [SerializeField] private ScriptableEvent checkEndGameEvent;
    [SerializeField] private IntValue power;
    [SerializeField] private GameObject vfxPrefab;
    private int currentHP = 100;
    public bool IsDead => currentHP <= 0;

    [ContextMenu("Kill")]
    void Kill()
    {
        DOTween.Kill(transform);
        LeanPool.Spawn(vfxPrefab, transform.position, transform.rotation);
        LeanPool.Despawn(gameObject);
        checkEndGameEvent.Raise();
    }

    public void Damage()
    {
        if (IsDead)
        {
            return;
        }

        currentHP -= power.Value * Random.Range(5, 20);
        
        if (IsDead)
        {
            Kill();
        }
    }

    public void OnSpawn()
    {
        currentHP = maxHP/(10 - power.Value);
    }

    public void OnDespawn()
    {
    }
}
