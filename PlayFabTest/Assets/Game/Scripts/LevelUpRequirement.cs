﻿using UnityEngine;
using UnityEngine.UI;

public class LevelUpRequirement : MonoBehaviour
{
    [SerializeField] private IntValue level;
    [SerializeField] private IntValue kills;
    [SerializeField] private Text textField;

    private void OnEnable()
    {
        level.OnValueChanged += UpdateUI;
        kills.OnValueChanged += UpdateUI;
    }

    private void OnDisable()
    {
        level.OnValueChanged -= UpdateUI;
        kills.OnValueChanged -= UpdateUI;
    }

    private void UpdateUI(int arg1, int arg2)
    {
        textField.text = $"{kills.Value}/{level.Value*10}";
    }
}
