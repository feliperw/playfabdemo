﻿using UnityEngine;

[CreateAssetMenu(fileName = "StringValue", menuName = "Scriptables/Variables/String")]
public class StringValue : ScriptableVariable<string>
{
}
