﻿using System.Collections;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

public class GameplayController : MonoBehaviour
{
    [SerializeField] private PlayFabStats playFabStats;
    [SerializeField] private Canvas canvas;
    [SerializeField] private Canvas loadingCanvas;
    
    private void OnEnable()
    {
        playFabStats.OnGetStatsSuccess += OnLoadedUserData;
        PlayFabLogin.OnSuccess += OnSuccess;
        PlayFabLogin.OnFail += OnFail;
    }

    private void OnDisable()
    {
        playFabStats.OnGetStatsSuccess -= OnLoadedUserData;
        PlayFabLogin.OnSuccess -= OnSuccess;
        PlayFabLogin.OnFail -= OnFail;
    }

    private void OnFail(PlayFabError error)
    {
        canvas.enabled = false;
    }

    private void OnSuccess(LoginResult result)
    {
        StartCoroutine(WaitAndGetData());
    }

    IEnumerator WaitAndGetData(float seconds = 1)
    {
        yield return new WaitForSeconds(seconds);
        playFabStats.GetStats();
    }

    private void OnLoadedUserData()
    {
        canvas.enabled = true;
    }
}
