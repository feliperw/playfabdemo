﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameLoopController : MonoBehaviour
{
    [SerializeField] List<Transform> InitialPath;
    [SerializeField] List<Transform> LoopPath;

    private Vector3[] StartPathWaypoints;
    private Vector3[] LoopPathWaypoints;

    private float StartPathLenght = 0;
    private float LoopPathLenght = 0;

    [SerializeField] GameObject prefab;
    [SerializeField] Button playButton;

    [SerializeField] private float speed = 1;
    
    [SerializeField] IntValue level;
    [SerializeField] ScriptableEvent checkEndGame;
    [SerializeField] PlayFabStats playFabStats;

    List<EnemyBehaviour> enemies = new List<EnemyBehaviour>();
    private int spawnQueue = 0;
    
    [ContextMenu("Spawn")]
    public void Spawn()
    {
        var go = Lean.Pool.LeanPool.Spawn(prefab).GetComponent<EnemyBehaviour>();
        enemies.Add(go);
        AddToPath(go.transform);
    }

    private void OnEnable()
    {
        checkEndGame.OnRaise += CheckEndGame;
    }

    private void OnDisable()
    {
        checkEndGame.OnRaise -= CheckEndGame;
    }

    private void CheckEndGame()
    {
        if(enemies.Count == 0) return;
        
        if(spawnQueue > 0) return;
        
        if(enemies.Count(e => !e.IsDead) > 0) return;
        
        playFabStats.GetReward(enemies.Count);
        enemies.Clear();
        spawnQueue = 0;
        playButton.interactable = true;
    }

    private void Awake()
    {
        StartPathWaypoints = InitialPath.Select(t => t.position).ToArray();
        LoopPathWaypoints = LoopPath.Select(t => t.position).ToArray();

        for (int i = 1; i < StartPathWaypoints.Length; i++)
        {
            StartPathLenght += Vector3.Distance(StartPathWaypoints[i - 1], StartPathWaypoints[i]);
        }
        
        for (int i = 1; i < LoopPathWaypoints.Length; i++)
        {
            LoopPathLenght += Vector3.Distance(LoopPathWaypoints[i - 1], LoopPathWaypoints[i]);
        }
    }

    private void AddToPath(Transform entity)
    {
        entity.position = InitialPath[0].position;
        entity.rotation = InitialPath[0].rotation;
        
        entity.DOPath(StartPathWaypoints, StartPathLenght/speed).SetEase(Ease.Linear).OnComplete(() => entity.DOPath(LoopPathWaypoints, LoopPathLenght/speed).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart));
    }

    public void StartGameLoop()
    {
        playButton.interactable = false;
        enemies.Clear();
        spawnQueue = Mathf.Min(Random.Range(0, 10) + 1, level.Value + 1);
        StartCoroutine(SpawnLoop());
    }

    IEnumerator SpawnLoop()
    {
        while (spawnQueue > 0)
        {
            Spawn();
            spawnQueue--;
            yield return new WaitForSeconds(Random.Range(1, 1.5f));
        }
        
    }
    
}
