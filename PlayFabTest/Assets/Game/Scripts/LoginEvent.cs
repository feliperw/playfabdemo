﻿using UnityEngine;

[CreateAssetMenu(fileName = "LoginEvent", menuName = "Scriptables/Events/Login Event")]
public class LoginEvent : ScriptableEventGeneric<string>
{
}
