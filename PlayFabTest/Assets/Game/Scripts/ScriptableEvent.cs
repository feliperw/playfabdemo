﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "NewEvent", menuName = "Scriptables/Events/Base Event")]
public class ScriptableEvent : ScriptableObject
{
    public Action OnRaise;

    public void Raise()
    {
        OnRaise?.Invoke();
    }
}
