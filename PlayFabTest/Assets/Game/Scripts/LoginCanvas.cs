﻿using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.UI;

public class LoginCanvas : MonoBehaviour
{
    [SerializeField] private LoginEvent loginEvent;
    [SerializeField] private Canvas canvas;
    [SerializeField] private InputField username;
    [SerializeField] private InputField password;

    private void OnEnable()
    {
        PlayFabLogin.OnSuccess += OnSuccess;
        PlayFabLogin.OnFail += OnFail;
    }
    
    private void OnDisable()
    {
        PlayFabLogin.OnSuccess -= OnSuccess;
        PlayFabLogin.OnFail -= OnFail;
    }

    private void OnFail(PlayFabError error)
    {
        canvas.enabled = true;
    }

    private void OnSuccess(LoginResult result)
    {
        canvas.enabled = false;
    }
    
    public void OnClickLogin()
    {
        if (string.IsNullOrEmpty(username.text) || string.IsNullOrEmpty(password.text))
        {
            loginEvent.Raise("");
            return;
        }
        
        var passHash = $"{username.text}{password.text}";
        loginEvent.Raise($"{username.text}_{Animator.StringToHash(passHash)}"); 
    }
}
