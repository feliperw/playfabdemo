﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewStringEvent", menuName = "Scriptables/Events/String Event")]
public class StringEvent : ScriptableEventGeneric<string>
{}
