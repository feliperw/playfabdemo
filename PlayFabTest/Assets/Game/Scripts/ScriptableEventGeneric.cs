﻿using System;
using UnityEngine;

public abstract class ScriptableEventGeneric<T> : ScriptableObject
{
    public Action<T> OnRaise;

    public void Raise(T value)
    {
        OnRaise?.Invoke(value);
    }
}