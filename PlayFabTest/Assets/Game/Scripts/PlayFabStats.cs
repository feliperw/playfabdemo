﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayFabStats", menuName = "PlayFab/PlayFabStats")]
public class PlayFabStats : ScriptableObject
{
    [SerializeField] IntValue Level;
    [SerializeField] IntValue Units;
    [SerializeField] IntValue Power;
    [SerializeField] IntValue Kills;
    
    [SerializeField] IntValue Gold;
    [SerializeField] IntValue Gems;

    [SerializeField] StringEvent ShowMessageEvent;
    [SerializeField] IntEvent StartRecruitTimer;

    public Action OnGetStatsSuccess;

    private const int MessageStatusReward = 1001;
    private const int MessageStatusStartRecruit = 1002;
    private const int MessageStatusFinishRecruit = 1003;
    private const int MessageStatusUpgradePower = 1004;
    private const int MessageStatusRecruitNotReady = 1501;
    private const int MessageStatusErrorMessage = 1500;

    public void GetStats()
    {
        PlayFabClientAPI.GetPlayerStatistics(new GetPlayerStatisticsRequest(),
            OnGetStatistics,
            OnError);
    }

    private void OnGetInventory(GetUserInventoryResult result)
    {
        Gold.Value = result.VirtualCurrency["SC"];
        Gems.Value = result.VirtualCurrency["HC"];
        OnGetStatsSuccess?.Invoke();
    }

    private void OnGetStatistics(GetPlayerStatisticsResult result)
    {
        foreach (var statistic in result.Statistics)
        {
            switch (statistic.StatisticName)
            {
                case "Level":
                    Level.Value = statistic.Value;
                    break;
                case "Units":
                    Units.Value = statistic.Value;
                    break;
                case "Power":
                    Power.Value = statistic.Value;
                    break;
                case "Kills":
                    Kills.Value = statistic.Value;
                    break;
                case "RecruitTime":
                    if (statistic.Value > 0)
                    {
                        CheckRecruit();
                    }
                    break;
            }
        }

        PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest(), OnGetInventory, OnError);
    }

    public void GetReward(int kills)
    {
        PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest()
        {
            FunctionName = "getReward",
            FunctionParameter = new {Kills = kills},
            GeneratePlayStreamEvent = false,
            
        },OnSuccess, OnError);
    }
    
    public void UpgradePower()
    {
        PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest()
        {
            FunctionName = "upgradePower",
            GeneratePlayStreamEvent = false,
            
        },OnSuccess, OnError);
    }
    
    public void RecruitUnit()
    {
        PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest()
        {
            FunctionName = "recruitNewUnit",
            GeneratePlayStreamEvent = false,
            
        },OnSuccess, OnError);
    }
    
    public void SpeedUpRecruit()
    {
        RecruitUnitNow(1);
    }
    
    public void CheckRecruit()
    {
        RecruitUnitNow(0);
    }
    
    void RecruitUnitNow(int speedUp)
    {
        PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest()
        {
            FunctionName = "recruitNewUnitNow",
            FunctionParameter = new {SpeedUp = speedUp},
            GeneratePlayStreamEvent = false,
            
        },OnSuccess, OnError);
    }
    
    private void OnError(PlayFabError error)
    {
        Debug.LogError(error.GenerateErrorReport());
    }
    
    private void OnSuccess(ExecuteCloudScriptResult result)
    {
        JsonObject jsonResult = result.FunctionResult as JsonObject;
        object outValue;

        if (!jsonResult.TryGetValue("Status", out outValue))
        {
            return;
        }

        var status = Convert.ToInt32((ulong)outValue);

        var message = "";
        switch (status)
        {
            case MessageStatusReward:
                message = "<b>You Got Rewards!</b>";
                if (jsonResult.TryGetValue("Gold", out outValue))
                {
                    var gold = Convert.ToInt32((ulong) outValue);
                    Gold.Value += gold;
                    message += $"\nGold: {gold}";
                }
                if (jsonResult.TryGetValue("Gem", out outValue))
                {
                    var gem = Convert.ToInt32((ulong) outValue);
                    Gems.Value += gem;
                    message += $"\nGem: {gem}";
                }
                if (jsonResult.TryGetValue("Level", out outValue))
                {
                    var lvl = Convert.ToInt32((ulong)outValue);
                    
                    if (Level.Value < lvl)
                    {
                        message += "\nLevel Up!";
                        Level.Value = lvl;
                    }
                }
                if (jsonResult.TryGetValue("Kills", out outValue))
                {
                    Kills.Value = Convert.ToInt32((ulong)outValue);
                }
                ShowMessageEvent.Raise(message);
                break;
            case MessageStatusErrorMessage:
                if (jsonResult.TryGetValue("Error", out outValue))
                {
                    ShowMessageEvent.Raise($"<b>ERROR!</b>\n{(string)outValue}");
                }
                break;
            case MessageStatusFinishRecruit:
                if (jsonResult.TryGetValue("Gem", out outValue))
                {
                    Gems.Value = Convert.ToInt32((ulong)outValue);
                }
                if (jsonResult.TryGetValue("Units", out outValue))
                {
                    Units.Value = Convert.ToInt32((ulong)outValue);
                }
                break;
            case MessageStatusStartRecruit:
                if (jsonResult.TryGetValue("Time", out outValue))
                {
                    StartRecruitTimer.Raise(Convert.ToInt32((ulong)outValue));
                }
                if (jsonResult.TryGetValue("SoftCurrency", out outValue))
                {
                    Gold.Value = Convert.ToInt32((ulong)outValue);
                }
                break;
            case MessageStatusUpgradePower:
                if (jsonResult.TryGetValue("Power", out outValue))
                {
                    Power.Value = Convert.ToInt32((ulong)outValue);
                }
                if (jsonResult.TryGetValue("SoftCurrency", out outValue))
                {
                    Gold.Value = Convert.ToInt32((ulong)outValue);
                }
                break;
            case MessageStatusRecruitNotReady:
                if (jsonResult.TryGetValue("Time", out outValue))
                {
                    StartRecruitTimer.Raise(Convert.ToInt32((ulong)outValue));
                }
                break;
        }
    }
}