﻿using UnityEngine;

[CreateAssetMenu(fileName = "LongValue", menuName = "Scriptables/Variables/Long")]
public class LongValue : ScriptableVariable<long>
{
}
