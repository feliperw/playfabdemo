﻿using System;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

public class PlayFabLogin : MonoBehaviour
{
    public static Action<PlayFabError> OnFail;
    public static Action<LoginResult> OnSuccess;

    const string LoginKey = "LoginKey";

    [SerializeField] private LoginEvent loginEvent;

    private void OnEnable()
    {
        loginEvent.OnRaise += Login;
    }
    
    private void OnDisable()
    {
        loginEvent.OnRaise -= Login;
    }

    public void Start()
    {
        if (string.IsNullOrEmpty(PlayFabSettings.TitleId)){
            PlayFabSettings.TitleId = "23DFF"; 
        }

        if (PlayerPrefs.HasKey(LoginKey))
        {
            Login(PlayerPrefs.GetString(LoginKey));
        }
    }
    
    private void Login(string customID)
    {
        PlayerPrefs.SetString(LoginKey, customID);
        
        if (string.IsNullOrEmpty(customID))
        {
            Login();
            return;
        }
        
        var request = new LoginWithCustomIDRequest {CustomId = customID, CreateAccount = true};
        PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);
    }

    private void Login()
    {        
#if UNITY_ANDROID
        var request = new LoginWithAndroidDeviceIDRequest {AndroidDeviceId = SystemInfo.deviceUniqueIdentifier, CreateAccount = true};
        PlayFabClientAPI.LoginWithAndroidDeviceID(request, OnLoginSuccess, OnLoginFailure);        
#elif UNITY_IOS
        var request = new LoginWithIOSDeviceIDRequest() {DeviceId = SystemInfo.deviceUniqueIdentifier, CreateAccount = true};
        PlayFabClientAPI.LoginWithIOSDeviceID(request, OnLoginSuccess, OnLoginFailure);
#endif
    }

    private void OnLoginSuccess(LoginResult result)
    {
        Debug.Log("Success Login");
        OnSuccess?.Invoke(result);
    }

    private void OnLoginFailure(PlayFabError error)
    {
        Debug.LogError(error.ErrorMessage);
        OnFail?.Invoke(error);
    }
}